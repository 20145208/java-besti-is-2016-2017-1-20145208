#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main()
{
	printf("my pid: %d \n", getpid());
	printf("my parent's pid: %d \n", getppid());
	return 0;
}
