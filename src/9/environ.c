#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	printf("PATH=%s\n", getenv("PATH"));
	setenv("PATH", "hello", 1);
	printf("PATH=%s\n", getenv("PATH"));
#if 0
	printf("PATH=%s\n", getenv("PATH"));
	setenv("PATH", "hellohello", 0);
	printf("PATH=%s\n", getenv("PATH"));


	printf("MY_VER=%s\n", getenv("MY_VER"));
	setenv("MY_VER", "1.1", 0);
	printf("MY_VER=%s\n", getenv("MY_VER"));
#endif
	return 0;
}
