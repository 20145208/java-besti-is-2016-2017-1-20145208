#include<stdio.h>

short addend1 = 1;
static int addend2 = 2;
const static long addend3 = 3;

static int g(int x)
{
	return x + addend1;
}  

static const int f(int x)
{
	return g(x + addend2);
}

int main(void)
{
	return f(8) + addend3;
}
